package web.controller;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import web.data.UserRepository;
import web.model.User;
import web.service.UserService;

@RestController
@RequestMapping(path = "/user", produces = "application/json")
@CrossOrigin(origins = "*")
public class UserController {
	@Autowired
	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<Iterable<User>> getAllusers() {
		return ResponseEntity.ok(userService.getAllusers());
	}
	@GetMapping("/{id}")
	public ResponseEntity<User> userById(@PathVariable("id") int id) {
		return ResponseEntity.ok(userService.userById(id));
	}
	@PostMapping(consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<User> createuser(@RequestBody User user) {
		return ResponseEntity.ok(userService.createuser(user));
	}
	@PutMapping("/{id}")
	public ResponseEntity<User> changeuser(@RequestBody User user) {
		return ResponseEntity.ok(userService.changeuser(user));
	}
	@Transactional
	@DeleteMapping("/{id}")
	public void deleteuser(@PathVariable("id") int id) {
		userService.deleteuser(id);
	}
}