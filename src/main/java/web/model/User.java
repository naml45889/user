package web.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Entity
@Table(name="user")
public class User {
	@Id
	private final int id;
	private final String name;
	private final String address;
	private final int age;
	private final String position;
	// 1- nu
	// 0 -nam
	private final int sex;
	private final Date dateOfBirth; 
	private double salary;
	private final String company;
	private final String status;
}
