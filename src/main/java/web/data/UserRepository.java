package web.data;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import web.model.User;
@Repository 
public interface UserRepository extends CrudRepository<User, String> {
	Optional<User> findById(int id);
	Optional<User> deleteById(int id);
}
